/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cruces_quetzalli;

/**
 *
 * @author Wally
 */
   public class Alumno {
    
    private String nombreMateria;
    private float examenParcial;
    private float practicas;
    private float examenFinal;
    private float asistencia;
    private String nombreAlumno;
    
     public Alumno (String nombreMateria){
         this.nombreMateria=nombreMateria;
         
     }
    
     public Alumno (float examenParcial){
         this.examenParcial=examenParcial;
         
     }
   
     public Alumno (String nombreAlumno, float examenParcial){
         this.nombreAlumno=nombreAlumno;
         this.examenParcial=examenParcial;
     }
    
     public Alumno (float practicas, float asistencia){
         this.practicas=practicas;
         this.asistencia=asistencia;
         
     }
     
     public Alumno (String nombreMateria, String nombreAlumno, float examenParcial){
         this.nombreMateria=nombreMateria;
         this.nombreAlumno=nombreAlumno;
         this.examenParcial=examenParcial;
     }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public float getExamenParcial() {
        return examenParcial;
    }

    public void setExamenParcial(float examenParcial) {
        
        this.examenParcial = examenParcial;
        if(examenParcial<=10){
        this.examenParcial = examenParcial;
        System.out.println("ya ajustamos el valor del examen parcial");}
        else{
            this.examenParcial= examenParcial;
            System.out.println("no puede a ver una calificacion mayor a 10");}
       
    }
   

    public float getPracticas() {
        return practicas;
    }

    public void setPracticas(float practicas) {
        this.practicas = practicas;
    }

    public float getExamenFinal() {
        return examenFinal;
    }

    public void setExamenFinal(float examenFinal) {
        this.examenFinal = examenFinal;
    }

    public float getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(float asistencia) {
        this.asistencia = asistencia;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }
    
       
    
}


package robles.misael.unidad1;
/**
 * @author MISAEL
 */
public class Alumno {
 
 private   String nombreMateria;
 private   String nombreAlumno;
 private   float examenParcial;
 private   float examenFinal;
 private   float practicas;
 private   float asistencia;
 
 /**
  * Este es un metodo de tipo set
  * @param examenParcial Tienes que poner dentro del parentesis
  * el valor que va a tomar el atributo examen parcial
  */
 
 public void setExamenParcial(float examenParcial){
     this.examenParcial=examenParcial;
     System.out.println("Ya ajustamos el valor del Examen Parcial");  
   
     if(examenParcial>=10){
         System.out.println("NO puede ser mayor de 10"); 
     }else{
         System.out.println("Ya ajustamos el valor del Examen Parcial"); 
     }
   
 }
 
 public float getExamenParcial(){
     return examenParcial;
}
 
}

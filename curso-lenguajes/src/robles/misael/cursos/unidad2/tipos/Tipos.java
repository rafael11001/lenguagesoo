
package robles.misael.cursos.unidad2.tipos;

public class Tipos {
     //static int algo;
    public static void main(String args[]){
        //declaramos un arreglo de enteros:
        /*forma de poder inicialisar el siclo for con el arroglo de incrementacion
         QUE UNO DE DESEA incrementar manualmente
        int[]arreglo1=new int[3];
        */
        int[]arreglo1={-20,7,8,54};
        /*
        METODO FEO Y BIEGO
        for(int i=0; i<arreglo1.length; i++){
             System.out.println(arreglo1[i]);
        }
                */
        //METODO NUEVO DE FOR para la implementacion maucho mas rapida
        //int x el tipo de dato sobre el que se va iterar(repetir una y otra vez) se relaciona al tipo de datos del arreglo
        //del lado derecha siempre va el arreglo para poder incrementar el tipo de dato
        for(int x: arreglo1){
            System.out.println(x);
        }
        
        //System.out.println(arreglo1[0]);
        //int algo;
        //System.out.println(algo);
    }
    
}

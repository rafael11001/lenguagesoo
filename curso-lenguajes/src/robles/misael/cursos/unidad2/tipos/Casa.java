
package robles.misael.cursos.unidad2.tipos;

public class Casa {
    static int numeroAlbercas;
    private int numeroCuartos;
    private int niveles;
    //supones getters y setters    

    public int getNumeroCuartos() {
        return numeroCuartos;
    }

    public void setNumeroCuartos(int numeroCuartos) {
        this.numeroCuartos = numeroCuartos;
    }

    public int getNiveles() {
        return niveles;
    }

    public void setNiveles(int niveles) {
        this.niveles = niveles;
    }
}

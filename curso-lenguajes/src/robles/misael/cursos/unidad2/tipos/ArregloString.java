
package robles.misael.cursos.unidad2.tipos;

public class ArregloString {

    public static void main(String args[]){
        //crear un areglo que contenga las frutas que te gusten
        String frutas[]={"platano","manzana","pera","melon","mango"};
        //crear un siclo for para iterar tus frutas
        for(String x: frutas){
            System.out.println(x);
        }
    } 
}

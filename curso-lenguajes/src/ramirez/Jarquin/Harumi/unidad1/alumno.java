/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ramirez.Jarquin.Harumi.unidad1;

/**
 *
 * @author Miguel Corona
 */
public class alumno {
    private String nombreMateria;
    private float examenParcial;
    private float practicas;
    private float examenFinal;
    private float asistencia;
    private String nombreAlumno;
    
    public alumno (){
        
    }
    
    public alumno (String nombreMateria){
        this.nombreMateria=nombreMateria;
    }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public float getExamenParcial() {
        return examenParcial;
    }

    public void setExamenParcial(float examenParcial) {
        this.examenParcial = examenParcial;
    }

    public float getPracticas() {
        return practicas;
    }

    public void setPracticas(float practicas) {
        this.practicas = practicas;
    }

    public float getExamenFinal() {
        return examenFinal;
    }

    public void setExamenFinal(float examenFinal) {
        this.examenFinal = examenFinal;
    }

    public float getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(float asistencia) {
        this.asistencia = asistencia;
    }
	
}


package com.capitulo4.tredsillo;

public class Intermadia implements Runnable {
    //Runnable es el tio de la clase
    //FORMA N.2 Inplementariado la interface Runnable
    public static void main(String args[]){
         //Paso1 crear el thread
        Runnable r = new Intermadia();
        Thread t1=new Thread(r);
        //Paso2 inicializamos
        t1.setName("impresion");
        t1.start();
        //Ejercicio crear otros Thread t2 y t3. Observar que se implemente el resultado
        Thread t2=new Thread(r);
        t2.setName("conexion");//lo pondremos a dormir mientras los demoas agandayan el run
        t2.start();
        Thread t3=new Thread(r);
        t3.setName("guardar"); 
        t3.start();
        
    }

    @Override
    public void run() {
        try{
        //Paso3 ejecucion
            if(Thread.currentThread().getName().equals("Impresion"))Thread.sleep(2000);
             if(Thread.currentThread().getName().equals("Conexion"))Thread.sleep(6000);  
              if(Thread.currentThread().getName().equals("guardar"))Thread.sleep(8000);  
         System.out.println(Thread.currentThread().getName());        
         System.out.println("Soy un Thread mediano");
        }catch(Exception e){}
        //catch(Exception e){} es un metodo de seguridad pero nunca de debe de despertar }
        //simepre tienen k estar dormido por medio de seguridad
  }
}

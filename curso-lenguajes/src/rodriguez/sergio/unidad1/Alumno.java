/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rodriguez.sergio.unidad1;

/**
 *
 * @author sergio
 */
public class Alumno {
      
   private  String nombreMateria;
   private float  examenParcial;
   private float practicas;
   private float examenFinal;
   private float asistencia;
   /**
    * 
    * este es un metodo de tipo set 
    * @param examenParcial 
    * tienes que poner dentro del parentesis el valor que va a tomar el atributo examen parcial
    */
   
   public void setExamenParcial(float examenParcial){
       if(examenParcial>10){
       System.out.println("no se permiten calificaciones mayores a 10");
       this.examenParcial=10;
       }
      if(examenParcial<0){System.out.println("no se permiten valores negativos");
           this.examenParcial=0; }
       else{   
         this.examenParcial=examenParcial;
         System.out.println("ya ajustamos el valor de examen parcial");
         
       }
   }
   public float getExamenParcial(){
   
   return examenParcial;
   }
   
    
}

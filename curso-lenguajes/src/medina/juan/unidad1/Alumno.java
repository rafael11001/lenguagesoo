/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medina.juan.unidad1;

/** Reglas del encapsulamiento
 * 
 * @author Jonny
 */
public class Alumno {
    private String nombreMateria;
    private float examenParcial;
    private float practicas;
    private float examenFinal;
    private float asistencia;
    private String nombreAlumno;
    //metodo de tipo set 
    //this se usa para distinguir el atributo
    /**
     * Este es un metodo de tipo set
     * @param examenParcial 
     * tienes que poner dentro del parentesis el valor que va a tomar el atributo examen parcial
     */
   public void setExamenParcial(float examenParcial){ //se delcara el timpo de dato deacuerdo al argumento que vamos a modificar
       this.examenParcial=examenParcial;//para buscar atributos ctrl+espacio
       if (examenParcial > 10){
           examenParcial=10;
       }
       else{
           if (examenParcial<=0){
           System.out.println("ya ajustamos el valor del examen parcial");
           
           }
           else{
               examenParcial=examenParcial;
           }
           
       }
       System.out.println("ya ajustamos el valor del examen parcial");
   }
   //el get siempre devuleve el valor del set siempre tiene que tener la palabra return
   /**
    * 
    * @return 
    * 
    */
   public float getExamenParcial(){
       return examenParcial;
   }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Franco.Arzate.Sandra.Maribel.unidad1;

/**
 *
 * @author FRANCO
 */
//Espero y este bien profe saludos
public class Alumno 
{
    private String nombreMateria;
    private float examenParcial;
    private float practicas;
    private float examenFinal;
    private float asistencia;
    private String nombreAlumno;
    
    
   	public Alumno() {
        }

    public Alumno(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public Alumno(String nombreMateria, float asistencia, String nombreAlumno) {
        this.nombreMateria = nombreMateria;
        this.asistencia = asistencia;
        this.nombreAlumno = nombreAlumno;
    }

    public Alumno(String nombreMateria, float examenParcial, float examenFinal, String nombreAlumno) {
        this.nombreMateria = nombreMateria;
        this.examenParcial = examenParcial;
        this.examenFinal = examenFinal;
        this.nombreAlumno = nombreAlumno;
    }

    public Alumno(float a, String f , float p, String m) 
    {
        nombreAlumno = "sandra";
        examenParcial = 7;
        examenFinal = 6;
        nombreMateria = "geografia";
    }

    public Alumno(float practicas, float asistencia, String nombreAlumno) {
        this.practicas = practicas;
        this.asistencia = asistencia;
        this.nombreAlumno = nombreAlumno;
    }


    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public float getExamenParcial() {
        return examenParcial;
    }

    public void setExamenParcial(float examenParcial) {
        if(examenParcial>10) this.examenParcial=10;
        else if(examenParcial>0) this.examenParcial=0;
        else this.examenParcial= examenParcial;
    }

    public float getPracticas() {
        return practicas;
    }

    public void setPracticas(float practicas) {
        this.practicas = practicas;
    }

    public float getExamenFinal() {
        return examenFinal;
    }

    public void setExamenFinal(float examenFinal) {
        this.examenFinal = examenFinal;
    }

    public float getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(float asistencia) {
        this.asistencia = asistencia;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    
    
    
}
